﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.IO;
using System.Text.RegularExpressions;

namespace jp.co.sundenshi.acereal.sdk.handtracking {

  /// <summary>
	/// ハンドトラッキングを認識、管理するクラス Hand tracking class and related controling functions
	/// </summary>
  public class HandTrackingSample : MonoBehaviour {
    //Androidアプリの呼びかけ用 for calling Android app functions
    private AndroidJavaObject plugin;
    private AndroidJavaClass unityPlayer;
    private AndroidJavaObject activity;
    private AndroidJavaObject javaObject;

    //ハンド位置検知用 for detecting hand positions
    private Regex regex = new Regex("Number of hands detected: 1");
    private Match match;
    private bool isHand = false;
    private Vector2[] fingerPosition = new Vector2[21];

    //ジェスチャー判定用 for gesture recognition
    private bool isGu = false;
    private bool isPa = false;

    //ユーザーに表示用 For showing to users
    [SerializeField] private bool fingerObjectsVisible=true;
    [SerializeField] private GameObject[] fingerObjects;
    [SerializeField] private float displayScaleX = 3;
    [SerializeField] private float displayScaleY = 5;

    //カメラの切り替え用
    [SerializeField] private CameraPosition UseCamera = CameraPosition.Center;  //0=Right,1=Left,2=Center
    [SerializeField] private ViewPosition viewPosition; //カメラプレビューの表示

    [SerializeField] private int CustomPositionFromLeft=300;
    [SerializeField] private int CustomPositionFromTop=300;

    // カメラの映像を表示する位置
    public enum ViewPosition {
      UpperLeft,
      UpperRight,
      LowerRight,
      LowerLeft,
      CustomPosition,
      NotDisplayed
    }

    // カメラの位置をIDに変更, 0=Right,1=Left,2=Center
    public enum CameraPosition {
      Right,
      Left,
      Center
    }

    // カメラ表示位置の座標
    private int previewTop = 10;
    private int previewLeft = 10;

    //	ジェスチャー結果が「キャンセル」の時のイベント
    [SerializeField] private UnityEvent eventFist;
    //	ジェスチャー結果が「ＯＫ」の時のイベント
    [SerializeField] private UnityEvent eventOpenHand;

    /// <summary>
    /// 開始時の処理 The entry point of the program
    /// </summary>
    private void Start() {
      //AndroidネイティブコードのUnity Java　Classを呼びかけ call to Android native code, Plugin Activity
      plugin = new AndroidJavaObject("jp.acereal.handreceiver.PluginActivity");
      unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
      activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
      plugin.Call("setContext", activity);

      //AndroidネイティブコードのhandBroadcastReceiverPluginを呼びかけ call to Android native code, handBroadcastReceiverPlugin
      javaObject = new AndroidJavaObject("jp.acereal.handreceiver.HandBroadcastReceiverPlugin", activity);
      javaObject.Call("configureReceiver");

      //3Dオブジェクトの表示/非表示
      if (!fingerObjectsVisible) {
        for (int i = 0; i < 5; i++) {
          fingerObjects[i].SetActive(false);
        }
      }

      SetViewPosition();

      StartRecognition();
    }

    /// <summary>
    /// カメラ映像の表示位置を計算する
    /// </summary>
    private void SetViewPosition() {
      if (viewPosition == ViewPosition.UpperLeft)  { previewTop = 10;  previewLeft = 10; }
      if (viewPosition == ViewPosition.UpperRight) { previewTop = 10;  previewLeft = 600; }
      if (viewPosition == ViewPosition.LowerRight) { previewTop = 700; previewLeft = 600; }
      if (viewPosition == ViewPosition.LowerLeft)  { previewTop = 700; previewLeft = 10; }
      if (viewPosition == ViewPosition.CustomPosition) { previewTop = CustomPositionFromTop; previewLeft = CustomPositionFromLeft; }
    }

    /// <summary>
    /// ハンドトラッキン背景グサービスを起動する Start the background service for hand tracking
    /// </summary>
    private void StartRecognition() {
      //設定ファイルを書き込むによりONする By writing to the setting file
      string FILE_PATH = "storage/emulated/0/jp.co.sundenshi/acereal/jp.co.sundenshi.acereal.handservice.hand_tracking_camera_setting";

      StreamWriter sr = System.IO.File.CreateText(FILE_PATH);
      sr.WriteLine("CAMERA_POWER=ON");
      sr.WriteLine("CAMERA_ID=" + (int) UseCamera);
      sr.WriteLine("OVERLAY=" + (viewPosition == ViewPosition.NotDisplayed ? "OFF" : "ON"));
      sr.WriteLine("POSITION=" + previewTop +","+ previewLeft);
      sr.Close();

      Debug.Log("SendStartTracking()");
    }

    /// <summary>
    /// 破棄された時の処理
    /// </summary>
    private void OnDestroy() {
      StopRecognition();
    }

    /// <summary>
    /// Unityアプリが実行していないとき、カメラもオフする Stop the camera when the Unity app is not active
    /// </summary>
    void OnApplicationPause(bool isOnPause) {
      if (isOnPause) {
        Debug.Log("OnApplicationPause onPause");
        StopRecognition();
      }
    }

      /// <summary>
      /// ハンドトラッキン背景グサービスを停止する Stop the background service for hand tracking
      /// </summary>
      private void StopRecognition() {
      //設定ファイルを書き込むによりONする By writing to the setting file
      string FILE_PATH = "storage/emulated/0/jp.co.sundenshi/acereal/jp.co.sundenshi.acereal.handservice.hand_tracking_camera_setting";

      StreamWriter sr = System.IO.File.CreateText(FILE_PATH);
      sr.WriteLine("CAMERA_POWER=OFF");
      sr.WriteLine("CAMERA_ID=" + (int)UseCamera);
      sr.WriteLine("OVERLAY=" + (viewPosition == ViewPosition.NotDisplayed ? "OFF" : "ON"));
      sr.Close();

      Debug.Log("StopRecognition()");
    }
    /// <summary>
    /// 表示のため、指の座標を引き取る処理 poll the finger positions for display
    /// </summary>
    private void Update() {
      //指の座標を引き取る poll the finger positions
      javaObject.Call("callbackToUnityJavaMethod", this.gameObject.name, "CallbackFromNativeAndroidMessage");
    }

    /// <summary>
    /// Android関数の呼び方 Calling native Android function
    /// <param name="strFromNativeAndroid"> 手の21ポイントの位置情報が一つStringでまとめる Return 21 points of the hand in one string </param>
    /// </summary>
    Vector2[] thumb = new Vector2[4];
    Vector2[] indexFinger = new Vector2[4];
    Vector2[] middleFinger = new Vector2[4];
    Vector2[] ringFinger = new Vector2[4];
    Vector2[] pinkyFinger = new Vector2[4];
    Vector2 wrist = new Vector2(0, 0);
    private void CallbackFromNativeAndroidMessage(string strFromNativeAndroid) {
      //ハード座標があるJSON Stringをデータを引き取る if a hand (and fingerObjs) is detected. Get finger positions from JSON
      if (regex.IsMatch(strFromNativeAndroid)) { //regex="Number of hand(s) detected: 1"
        isHand = true;
        foreach (Match match in Regex.Matches(strFromNativeAndroid, @"Position (\d+): \((0\.\d+)+\, (0\.\d+)\)", RegexOptions.IgnoreCase)) {
          fingerPosition[int.Parse(match.Groups[1].Value)] = new Vector2(displayScaleX * float.Parse(match.Groups[3].Value),
                                                                          displayScaleY * float.Parse(match.Groups[2].Value));
        }
        //指の名前に分けていく　Update the object positions
        wrist = new Vector2(fingerPosition[0].x, fingerPosition[0].y);
        int i;
        for (i = 0; i < 4; i++) {
          thumb[i] = new Vector2(fingerPosition[i+1].x, fingerPosition[i+1].y);
          indexFinger[i] = new Vector2(fingerPosition[i+5].x, fingerPosition[i+5].y);
          middleFinger[i] = new Vector2(fingerPosition[i+9].x, fingerPosition[i+9].y);
          ringFinger[i] = new Vector2(fingerPosition[i+13].x, fingerPosition[i+13].y);
          pinkyFinger[i] = new Vector2(fingerPosition[i+17].x, fingerPosition[i+17].y);
        }

        //オブジェクト位置を更新　Update the object positions
        if (fingerObjectsVisible) {
          fingerObjects[0].transform.position = new Vector3(thumb[3].x, thumb[3].y, 0);
          fingerObjects[1].transform.position = new Vector3(indexFinger[3].x, indexFinger[3].y, 0);
          fingerObjects[2].transform.position = new Vector3(middleFinger[3].x, middleFinger[3].y, 0);
          fingerObjects[3].transform.position = new Vector3(ringFinger[3].x, ringFinger[3].y, 0);
          fingerObjects[4].transform.position = new Vector3(pinkyFinger[3].x, pinkyFinger[3].y, 0);
        }

        //親指先と人差し指先の距離によって、入力判別する(赤色になる) trigger input (red colour) depending on the distance between fingers
        float distance = Vector3.Distance(fingerObjects[0].transform.position, fingerObjects[1].transform.position);
        if (distance < 0.4) {
          fingerObjects[0].GetComponent<Renderer>().material.color = Color.red;
          fingerObjects[1].GetComponent<Renderer>().material.color = Color.red;
        }
        else {
          fingerObjects[0].GetComponent<Renderer>().material.color = Color.white;
          fingerObjects[1].GetComponent<Renderer>().material.color = Color.white;
        }

        GestureRegcognition(strFromNativeAndroid);

      } else {
        isHand = false;
      }
    }

    public bool IsHandVisible() {
      return isHand;
    }

    // 指のステータス Finger states
    private bool thumbIsOpen = false;
    private bool indexFingerIsOpen = false;
    private bool middleFingerIsOpen = false;
    private bool ringFingerIsOpen = false;
    private bool pinkyFingerIsOpen = false;
    private float fixKeyPoint;
    private Regex regexGu = new Regex("Gesture=GU");
    private Regex regexPa = new Regex("Gesture=PA");
    private void GestureRegcognition(string strFromNativeAndroid) {
      isGu = false; isPa=false;
      if (regexGu.IsMatch(strFromNativeAndroid)) {
        isGu = true;
      }
      if (regexPa.IsMatch(strFromNativeAndroid)) {
        isPa = true;
      }
      
      fixKeyPoint = thumb[1].x;
      if (        indexFinger[0].x < ringFinger[0].x &&
                  thumb[2].x < fixKeyPoint && thumb[3].x < fixKeyPoint) {
        thumbIsOpen = true;
      } else if ( indexFinger[0].x > ringFinger[0].x &&
                  thumb[2].x > fixKeyPoint && thumb[3].x > fixKeyPoint) {
        thumbIsOpen = true;
      } else { thumbIsOpen = false;　}
      fixKeyPoint = indexFinger[1].y;
      if (indexFinger[2].y > fixKeyPoint && indexFinger[3].y > fixKeyPoint) {
        indexFingerIsOpen = true;
      } else { indexFingerIsOpen = false; }
      fixKeyPoint = middleFinger[1].y;
      if (middleFinger[2].y > fixKeyPoint && middleFinger[3].y > fixKeyPoint) {
        middleFingerIsOpen = true;
      } else { middleFingerIsOpen = false; }
      fixKeyPoint = ringFinger[1].y;
      if (ringFinger[2].y > fixKeyPoint && ringFinger[3].y > fixKeyPoint) {
        ringFingerIsOpen = true;
      } else { ringFingerIsOpen = false; }
      fixKeyPoint = pinkyFinger[1].y;
      if (pinkyFinger[2].y > fixKeyPoint && pinkyFinger[3].y > fixKeyPoint) {
        pinkyFingerIsOpen = true;
      } else { pinkyFingerIsOpen = false; }
      // ジェスチャー認識 Hand gesture recognition
      if (isPa){
        Debug.Log("OPEN HAND");
        eventOpenHand.Invoke();
      } else if (isGu){
        Debug.Log("FIST");
        eventFist.Invoke();
      } else if (!indexFingerIsOpen && middleFingerIsOpen && ringFingerIsOpen && pinkyFingerIsOpen && isThumbNearIndexFinger()) {
        Debug.Log("OK");
      // ジェスチャー認識例 gesture recognition possibilities
      //} else if (!thumbIsOpen && indexFingerIsOpen && middleFingerIsOpen && ringFingerIsOpen && pinkyFingerIsOpen) {
      //  Debug.Log("FOUR");
      //} else if (!thumbIsOpen && indexFingerIsOpen && middleFingerIsOpen && ringFingerIsOpen && !pinkyFingerIsOpen) {
      //  Debug.Log("THREE");
      //} else if (!thumbIsOpen && indexFingerIsOpen && middleFingerIsOpen && !ringFingerIsOpen && !pinkyFingerIsOpen) {
      //  Debug.Log("TWO");
      //} else if (!thumbIsOpen && indexFingerIsOpen && !middleFingerIsOpen && !ringFingerIsOpen && !pinkyFingerIsOpen) {
      //  Debug.Log("ONE");
      } else {
        Debug.LogFormat("NO HAND POSE.Fingers:[{0}][{1}][{2}][{3}][{4}]", thumbIsOpen, indexFingerIsOpen, middleFingerIsOpen, ringFingerIsOpen, pinkyFingerIsOpen);
      }
    }

    /// <summary>
    /// 親指の位置と人差し指の位置の距離が近いかどうか
    /// </summary>
    bool isThumbNearIndexFinger() {
      float distance = Vector2.Distance(fingerPosition[4], fingerPosition[8]);
      if (distance < 0.4) {
        return true;
      }
      else {
        return false;
      }
    }

    /// <summary>
    /// ジェスチャーはグーか　Is it a fist?
    /// </summary>
    public bool isFist() {
      return isGu;
    }

    /// <summary>
    /// ジェスチャーはパーか　Is it an open hand?
    /// </summary>
    public bool isOpenHand() {
      return isPa;
    }

    /// イベントＯＫ時に起動する処理の追加
    /// </summary>
    /// <param name="call">追加する処理</param>
    public void AddListenerToEventOk(UnityAction call) {
      eventOpenHand.AddListener(call);
    }

    /// <summary>
    /// イベントＣａｎｃｅｌ時に起動する処理の追加
    /// </summary>
    /// <param name="call">追加する処理</param>
    public void AddListenerToEventCancel(UnityAction call) {
      eventFist.AddListener(call);
    }
  }
}