package jp.acereal.handreceiver;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

/// <summary>
/// UnityをAndroid関数を呼べるように
/// Allows Unity to call Android functions
/// </summary>
public class PluginActivity  extends UnityPlayerActivity {
    private Context context;

    public void setContext(Context context) {
        this.context = context;
        Log.d("setContext()", "started");
    }

}