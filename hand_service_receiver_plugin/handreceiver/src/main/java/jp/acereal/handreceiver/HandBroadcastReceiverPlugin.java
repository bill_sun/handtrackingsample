package jp.acereal.handreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.unity3d.player.UnityPlayer;

/// <summary>
/// ブロードキャスト受信処理、AARでUnityのPlug-inになる
/// Handles broadcast operations, output as an AAR file for an Unity Plugin
/// </summary>
public class HandBroadcastReceiverPlugin extends BroadcastReceiver {

    private static String textToUnity = "default text";
    private static HandBroadcastReceiverPlugin instance;
    private static Context context;

    public HandBroadcastReceiverPlugin(){}

    public HandBroadcastReceiverPlugin(Context context){
        this.context = context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String sentIntentText = intent.getStringExtra(Intent.EXTRA_TEXT);

        //System.out.println("onReceive(): sentIntentText to Unity="+sentIntentText);
        if (sentIntentText != null) {
            textToUnity = sentIntentText;
        }
    }

    public  void callbackToUnityJavaMethod(String gameObject, String methodName) {
        UnityPlayer.UnitySendMessage(gameObject, methodName, textToUnity);
    }

    private void configureReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("jp.co.sundenshi.acereal.handservice.IntentToUnity");
        context.registerReceiver(this, filter);
        System.out.println("registerReceiver(): jp.co.sundenshi.acereal.handservice.IntentToUnity");
    }

}