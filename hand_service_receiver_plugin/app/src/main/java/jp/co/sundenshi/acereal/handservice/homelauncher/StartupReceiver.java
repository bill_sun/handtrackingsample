package jp.co.sundenshi.acereal.handservice.homelauncher;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import jp.co.sundenshi.acereal.handservice.MainActivity;

/**
 * OS起動時にMainActivityを呼び出すためのクラス
 *
 * Auther:	akumagai
 * Date:	2017/06/08
 */

public class StartupReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentActivity = new Intent(context, MainActivity.class);
        intentActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentActivity.putExtra(Intent.ACTION_BOOT_COMPLETED, true);
        context.startActivity(intentActivity);
    }
}
