package jp.co.sundenshi.acereal.handservice;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

/// <summary>
/// カメラ映像をスレッドでBMPに変換する処理
/// Turn camera image into bitmap in a separate thread
/// </summary>
public class BmpProducer extends Thread {

    CustomFrameAvailableListener customFrameAvailableListener;

    Bitmap bmp;

    BmpProducer(Context context){
        bmp = BitmapFactory.decodeResource(context.getResources(), R.id.preview_display_layout);
        bmp = Bitmap.createScaledBitmap(bmp, 640,480,true);
        start();
    }

    public void setCustomFrameAvailableListener(CustomFrameAvailableListener customerFrameAvailableListner){
        this.customFrameAvailableListener = customerFrameAvailableListner;
    }

    public static final String TAG = "BmpProducer";

    @Override
    public void run() {
        super.run();
        while((true)){
            if (bmp==null  || customFrameAvailableListener == null)
                continue;
            Log.d(TAG, "Writing frame with bitmap BMP");
            customFrameAvailableListener.onFrame(bmp);

            try{
                Thread.sleep(10);
            } catch(Exception e){
                Log.d(TAG,e.toString());
            }
        }
    }
}
