package jp.co.sundenshi.acereal.handservice;

import android.graphics.Bitmap;

public interface CustomFrameAvailableListener {
    public void onFrame(Bitmap bitmap);
}
