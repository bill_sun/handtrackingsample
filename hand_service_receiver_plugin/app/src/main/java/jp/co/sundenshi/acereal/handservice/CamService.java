package jp.co.sundenshi.acereal.handservice;

import android.app.Activity;
import android.app.NotificationManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Debug;
import android.os.Environment;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmark;
import com.google.mediapipe.formats.proto.LandmarkProto.NormalizedLandmarkList;
//import com.google.mediapipe.components.CameraHelper;
import com.google.mediapipe.components.ExternalTextureConverter;
import com.google.mediapipe.components.FrameProcessor;
import com.google.mediapipe.components.PermissionHelper;
import com.google.mediapipe.framework.AndroidAssetUtil;
import com.google.mediapipe.framework.PacketGetter;
import com.google.mediapipe.glutil.EglManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

import android.Manifest;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Size;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.co.sundenshi.acereal.handservice.homelauncher.Utility;

/// <summary>
/// フォアグランドサービスとしてカメラデータをアクセス、手の位置を分析した結果を出力
/// Access camera data as a foreground service, identify and output hand position
/// </summary>
public class CamService extends Service {
    private static final String TAG = "CamService";
    private static final String BINARY_GRAPH_NAME = "multihandtrackinggpu.binarypb";
    private static final String INPUT_VIDEO_STREAM_NAME = "input_video";
    private static final String OUTPUT_VIDEO_STREAM_NAME = "output_video";
    private static final String OUTPUT_LANDMARKS_STREAM_NAME = "multi_hand_landmarks";
    // Flips the camera-preview frames vertically before sending them into FrameProcessor to be
    // processed in a MediaPipe graph, and flips the processed frames back when they are displayed.
    // This is needed because OpenGL represents images assuming the image origin is at the bottom-left
    // corner, whereas MediaPipe in general assumes the image origin is at top-left.
    //表示するためカメラの画像を縦を逆にします
    private static final boolean FLIP_FRAMES_VERTICALLY = true;

    static {
        // Load all native libraries needed by the app.
        System.loadLibrary("mediapipe_jni");
        System.loadLibrary("opencv_java3");
    }

    // where the camera-preview frames can be accessed.
    private SurfaceTexture previewFrameTexture;
    // that displays the camera-preview frames processed by a MediaPipe graph.
    private SurfaceView previewDisplayView;
    // Creates and manages an EGLContext.
    private EglManager eglManager;
    // Sends camera-preview frames into a MediaPipe graph for processing, and displays the processed
    // frames onto a {@link Surface}.
    private FrameProcessor processor;
    // Converts the GL_TEXTURE_EXTERNAL_OES surfaceTexture from Android camera into a regular surfaceTexture to be
    // consumed by FrameProcessor and the underlying MediaPipe graph.
    private MyExternalTextureConverter converter;

    public static final int NOTIFICATION_BUTTON_REQ_CODE = 1001; //just a req id, just need to be unique
    public static final int ONGOING_NOTIFICATION_ID = 1002;
    public static final String CHANNEL_ID = "cameraService";
    // UI
    private WindowManager wm = null;
    private View mainActivityView = null;
    private TextureView textureView = null;
    private ViewGroup viewGroup = null;

    //Camera2関係　Camera2 related
    private CameraManager cameraManager = null;
    private ArrayList<Integer> cameraIdListInt = new ArrayList<Integer>();
    private int camId = 2; //デフォルトは真ん中のカメラ Default to the middle camera (camID=2)
    private CameraDevice cameraDevice = null;
    private CaptureRequest.Builder previewRequestBuilder;
    private CaptureRequest captureRequest = null;
    private CameraCaptureSession cameraCaptureSession;

    // カメラデータをBMPに変換 Handles camera data to bitmap (BMP)
    BmpProducer bitmapProducer;

    //テクスチャ関係 Texture Surface related
    private SurfaceTexture previewFrameTexture2 = null;
    private Surface previewSurface = null;
    private Size previewImageSize = new Size(640, 360);
    private ImageReader imageReader;

    //ブロードキャストメッセージを送る send broadcast message
    private final Handler sendHandler = new Handler();
    public static final int SEND_INTERVAL = 100;  //send message every 100 ms (0.1sec)
    String multiHandLandmarksStr = "Empty position";

    //ファイルを常時監視する constantly check the file for modification
    private static final String mExtDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/jp.co.sundenshi/acereal/";//ファイルパスのルート file path root
    private static final String OUTER_COMMAND_FILE = "jp.co.sundenshi.acereal.handservice.hand_tracking_camera_setting";
    File commandFile;
    private static final int CHECK_INTERVAL = 1000;  //send message every 1000 ms (1sec)
    private long lastModifiedTime = 0;
    private int userCamId = 2;
    private boolean overlayIsOn = true;
    private Handler fileHandler = new Handler(Looper.getMainLooper()) {
        public void handleMessage(Message inputMessage) {
            super.handleMessage(inputMessage); //pass along other UI messages
        }
    };

    //ステータスバーで状態表示 Show current status at the notification bar
    Notification notification;
    NotificationCompat.Builder builder;
    NotificationManager notificationManager;

    //グーとパーを認識てきたら、サービス出力する
    boolean isGu = false;
    boolean isPa = false;

    @Override
    public void onCreate() {
        //create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //Android O/8, api level 26, or above
            notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                    .setContentTitle("Hand Tracking Service")
                    .setContentText("Running")
                    .build();
        } else {
            // between API level 23-26 up to Android N/7, just no channel ID needed
            notification = new Notification.Builder(getApplicationContext())
                    .setContentTitle("Hand Tracking Service")
                    .setContentText("Hand Tracking Service OFF")
                    .setSmallIcon(R.drawable.hand_not_tracking)
                    .setColor(Color.BLACK)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .build();
        }

        //make the notification into service to foreground
        startForeground(ONGOING_NOTIFICATION_ID, notification);

    }

    /// <summary>
    /// 常時ファイルを監視し、変更があったら、CamServiceをON/OFFする
    /// </summary>
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        System.out.println("[DEBUG] Inside : CamService->onStartCommand");

        if (intent != null) {
            fileHandler.removeCallbacks(fileCheck);
            fileHandler.postDelayed(fileCheck, CHECK_INTERVAL);

            // send broadcast message
            sendHandler.removeCallbacks(sendData);
            sendHandler.postDelayed(sendData, SEND_INTERVAL);
        }

        return START_STICKY;
    }

    private void startCameraService() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            Log.d("[ERROR]", "Camera permission DENIED. Restart the app.");
        } else {
            Log.d("[START]", "startCameraService(). Camera permission OK");

            previewDisplayView = new SurfaceView(this);
            setupPreviewDisplayView();
            // Initialize asset manager so that MediaPipe native libraries can access the app assets, e.g.,binary graphs.
            AndroidAssetUtil.initializeNativeAssetManager(this);
            eglManager = new EglManager(null);
            processor =
                    new FrameProcessor(
                            this,
                            eglManager.getNativeContext(),
                            BINARY_GRAPH_NAME,
                            INPUT_VIDEO_STREAM_NAME,
                            OUTPUT_VIDEO_STREAM_NAME);
            processor.getVideoSurfaceOutput().setFlipY(FLIP_FRAMES_VERTICALLY);
            processor.addPacketCallback(
                    OUTPUT_LANDMARKS_STREAM_NAME,
                    (packet) -> {
                        Log.d(TAG, "Received multi-hand position packet.");
                        List<NormalizedLandmarkList> multiHandLandmarks =
                                PacketGetter.getProtoVector(packet, NormalizedLandmarkList.parser());
                        isGUorPA(multiHandLandmarks);
                        Log.d(
                                TAG,
                                "[TS:"
                                        + packet.getTimestamp()
                                        + "] "
                                        + getMultiHandLandmarksDebugString(multiHandLandmarks));

                    });

            converter = new MyExternalTextureConverter(eglManager.getContext());
            converter.setFlipY(FLIP_FRAMES_VERTICALLY);
            converter.setConsumer(processor);
            permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                startCamera();
            }
        }
    }

    private Runnable sendData = new Runnable() {
        @Override
        public void run() {
            Intent sendIntent = new Intent();
            sendIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_FROM_BACKGROUND | Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
            sendIntent.setAction("jp.co.sundenshi.acereal.handservice.IntentToUnity");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "Landmarks:" + multiHandLandmarksStr);
            sendBroadcast(sendIntent);
            sendHandler.removeCallbacks(this);
            sendHandler.postDelayed(this, SEND_INTERVAL);
        }
    };

    private void onResume() {  // MOVE TO onStartCommand
        converter = new MyExternalTextureConverter(eglManager.getContext());
        converter.setFlipY(FLIP_FRAMES_VERTICALLY);
        converter.setConsumer(processor);

        startProducer();
    }

    private void startProducer() {
        bitmapProducer = new BmpProducer(this);
        previewDisplayView.setVisibility(View.VISIBLE);
    }

    private void setupPreviewDisplayView() {
        // Android O/8, api level 26, or above
        int type;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;
        }

        wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        final WindowManager.LayoutParams params = new WindowManager.LayoutParams(type,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        //Hand Tracking's code
        previewDisplayView.setVisibility(View.GONE);
        LayoutInflater inflater = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE));
        mainActivityView = inflater.inflate(R.layout.activity_main, null);
        textureView = mainActivityView.findViewById(R.id.texPreview);
        viewGroup = mainActivityView.findViewById(R.id.preview_display_layout);
        viewGroup.addView(previewDisplayView);

        previewDisplayView
                .getHolder()
                .addCallback(
                        new SurfaceHolder.Callback() {
                            @Override
                            public void surfaceCreated(SurfaceHolder holder) {
                                System.out.println("[DEBUG] Callback surfaceCreated");
                                processor.getVideoSurfaceOutput().setSurface(holder.getSurface());
                            }

                            @Override
                            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                                System.out.println("[DEBUG] Callback surfaceChanged");
                                bitmapProducer.setCustomFrameAvailableListener(converter);
                            }

                            @Override
                            public void surfaceDestroyed(SurfaceHolder holder) {
                                System.out.println("[DEBUG] Callback surfaceDestroyed");
                                processor.getVideoSurfaceOutput().setSurface(null);
                            }
                        });
        wm.addView(mainActivityView, params);
    }

    private void startCamera() {

        if (textureView.isAvailable())
            initCam();//textureView.getWidth(), textureView.getHeight());
        else
            textureView.setSurfaceTextureListener(surfaceTextureListener);
    }

    private void initCam() {
        Log.e("START", "initCam");

        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        //ユーザーのcamId指定があるかどうか、あればそれに設定、なければ真ん中(camID=2)に設定
        //check whether the user has set a camId to be used, if so, use it; If not, set to middle cam (camId=2)
        userCamId = 2;
        if (!android.os.Build.PRODUCT.equals("AceReal0100")) {
            userCamId = 0; //AceReal以外はcamId=0,デバッグ用 camId=0 for debugging on phone
        } else {
            //もしユーザーカメラファイルが存在しないなら、デフォルトはuserCamId、それをファイルを作成
            //if there is no hand_tracking_camera_setting file, make one default to userCamId
            String strFilename = OUTER_COMMAND_FILE;
            commandFile = new File(mExtDir + strFilename);
            try {
                if (!commandFile.exists()) {
                    initCameraFile(commandFile);
                } else {  //ユーザーカメラファイルが存在するので、読み込みます user camera file exists, reads it
                    FileInputStream fis = new FileInputStream(commandFile);
                    StringBuffer fileContent = new StringBuffer("");
                    byte[] buffer = new byte[1024];
                    int n;
                    while ((n = fis.read(buffer)) != -1) {
                        fileContent.append(new String(buffer, 0, n));
                    }
                    String strFileContent = fileContent.toString();
                    Log.d("DEBUG", strFileContent);
                    Pattern pattern = Pattern.compile("CAMERA_ID=[0-9]{1}");
                    Matcher matcher = pattern.matcher(strFileContent);
                    while (matcher.find()) {
                        Log.d("DEBUG", "FOUND: " + matcher.group() + ", starting at index: " +
                                matcher.start() + ", ends at : " + matcher.end());
                        userCamId = Integer.parseInt(strFileContent.substring(matcher.end() - 1, matcher.end()));
                    }
                    pattern = Pattern.compile("OVERLAY=OFF");
                    matcher = pattern.matcher(strFileContent);
                    while (matcher.find()) {
                        Log.d("DEBUG", "FOUND: " + matcher.group() + ", starting at index: " +
                                matcher.start() + ", ends at : " + matcher.end());
                        overlayIsOn = false;
                    }
                }
            } catch (IOException e) {
                Log.e("ERROR", e.toString());
            }
        }
        //camIdとLENS_FACINGによりカメラを指定 select camera based on camID and LENS_FACING
        try {
            cameraIdListInt.clear();
            String[] cameraIdsStr = cameraManager.getCameraIdList();
            for (int i = cameraIdsStr.length - 1; i > 0; i--) {
                this.cameraIdListInt.add(Integer.parseInt(cameraIdsStr[i]));
                // get the first camera with lens facing the back
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraIdsStr[i]);
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing == CameraCharacteristics.LENS_FACING_BACK
                        && i == userCamId) {
                    camId = i;
                    break;
                }
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        // open camera. If there's no permission to use the camera, display an error message on toast
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Camera permission OK", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Camera permission DENIED. Restart the app.", Toast.LENGTH_LONG).show();
        }

        // open camera
        try {
            cameraManager.openCamera(Integer.toString(camId), openCamCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    private CameraDevice.StateCallback openCamCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice currentCameraDevice) {
            System.out.println("[DEBUG] camera onOpened");

            cameraDevice = currentCameraDevice;

            List<Surface> outputs = new ArrayList<>();
            try {
                previewRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                previewFrameTexture = textureView.getSurfaceTexture();
                if (previewFrameTexture == null) {
                    System.out.println("[ERROR] previewFrameTexture == null");
                } else {
                    previewFrameTexture.setDefaultBufferSize(previewImageSize.getWidth(), previewImageSize.getHeight());

                    previewSurface = new Surface(previewFrameTexture);
                    outputs.add(previewSurface);
                    previewRequestBuilder.addTarget(previewSurface);

                    imageReader = ImageReader.newInstance(previewImageSize.getWidth(), previewImageSize.getHeight(), ImageFormat.JPEG, 2);
                    imageReader.setOnImageAvailableListener(imageListener, null);
                    outputs.add(imageReader.getSurface());
                    previewRequestBuilder.addTarget(imageReader.getSurface());

                    cameraDevice.createCaptureSession(outputs, configuredCallback, null);
                }

            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice camera) {
            System.out.println("[DEBUG] camera onDisconnected");
        }

        @Override
        public void onError(@NonNull CameraDevice camera, int error) {
            System.out.println("[DEBUG] camera open onError");
        }
    };

    private final ImageReader.OnImageAvailableListener imageListener = new ImageReader.OnImageAvailableListener() {
        /**
         * カメラの画像取得したときの処理
         * @param reader
         */
        @Override
        public void onImageAvailable(ImageReader reader) {

            // ImageReaderから画像データ(byte配列)を取得 (YUV)
            Image image = reader.acquireLatestImage();
            if (image == null) {
                System.out.println("[ERROR] image null");
                return;
            }
            System.out.println("[DEBUG] onImageAvailable. Got image: " + image.getWidth() + " x " + image.getHeight());
            Image.Plane[] planes = image.getPlanes();
            if (planes == null) {
                System.out.println("[DEBUG] plane open onError");
                return;
            }
            ByteBuffer imageBuf = image.getPlanes()[0].getBuffer();
            final byte[] imageBytes = new byte[imageBuf.remaining()];
            imageBuf.get(imageBytes);
            image.close();

            final Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);


            //TODO take non-blocking YUV camera data and generate BMP via shader to improve latency

            // this put a BMP to FrameProcessor, but the graph expects a SurfaceTexture "Packet type mismatch on calculator outputting to stream "input_video": The Packet stores "mediapipe::ImageFrame", but "mediapipe::GpuBuffer" was requested."
            Long tsLong = System.currentTimeMillis(); //new Date().getTime();
            processor.setVideoInputStreamCpu(INPUT_VIDEO_STREAM_NAME);
            converter.onFrame(bitmap);

        }
    };

    private CameraCaptureSession.StateCallback configuredCallback = new CameraCaptureSession.StateCallback() {
        /**
         * カメラのキャプチャセッションが作成できたときの処理
         * @param session
         */
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
            System.out.println("[DEBUG] camera onConfigured");
            cameraCaptureSession = session;
            try {
                if (null == cameraDevice) {
                    return;
                }

                try {
                    // Now we can start capturing
                    captureRequest = previewRequestBuilder.build();
                    cameraCaptureSession.setRepeatingRequest(captureRequest, capturedCallback, null);


                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            } finally {

            }
        }

        /**
         * カメラのキャプチャセッション作成に失敗した時の処理
         * @param session
         */
        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            System.out.println("camera configured failed");
        }
    };


    private final TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            Log.d("[DEBUG]", "onSurfaceTextureAvailable");
            //when a texture (surface texture) is available, the TextureView is notified, and start the camera
            initCam();

            if (!overlayIsOn) {
                textureView.setVisibility(View.GONE);
                Log.d("[DEBUG]", "Overlay is OFF");
            }else{
                textureView.setVisibility(View.VISIBLE);
                Log.d("[DEBUG]", "Overlay is ON");
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            Log.d("[DEBUG]", "onSurfaceTextureSizeChanged");
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            Log.d("[DEBUG]", "onSurfaceTextureDestroyed");
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
            Log.d("[DEBUG]", "onSurfaceTextureUpdated");
        }
    };

    private CameraCaptureSession.CaptureCallback capturedCallback = new CameraCaptureSession.CaptureCallback() {
        /**
         * カメラの画像取得したときの処理
         * @param session
         * @param request
         * @param result
         */
        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
            System.out.println("[DEBUG] Callback onCaptureCompleted");
            super.onCaptureCompleted(session, request, result);
            // nothing needs to be done
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (previewDisplayView!=null) {
            previewDisplayView.setVisibility(View.GONE);
        }

        try {
            if (converter != null) {
                converter.close();
                converter = null;
            }
            if (cameraCaptureSession != null) {
                cameraCaptureSession.close();
                cameraCaptureSession = null;
            }
            if (cameraDevice != null) {
                cameraDevice.close();
                cameraDevice = null;
            }
            if (imageReader != null) {
                imageReader.close();
                imageReader = null;
            }
            if (commandFile!=null){
                initCameraFile(commandFile);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mainActivityView != null) {
            if (mainActivityView.getWindowToken() != null){
                wm.removeView(mainActivityView);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Description:	指の位置21ポイントをテキストでログに出す
     */
    private String getMultiHandLandmarksDebugString(List<NormalizedLandmarkList> multiHandLandmarks) {

        String outText = "start of the function";
        Log.d("HandPositionStr", outText);

        multiHandLandmarksStr = "Number of hands detected: " + multiHandLandmarks.size() + ".\n";
        int handIndex = 0;

        for (NormalizedLandmarkList landmarks : multiHandLandmarks) {
            multiHandLandmarksStr +=
                    "Hand positions for hand[" + handIndex + "]: " + landmarks.getLandmarkCount() + ".";
            int landmarkIndex = 0;
            for (NormalizedLandmark landmark : landmarks.getLandmarkList()) {
                multiHandLandmarksStr +=
                        "\t\tPosition "
                                + landmarkIndex
                                + ": ("
                                + landmark.getX()
                                + ", "
                                + landmark.getY()
                                + ")";
                ++landmarkIndex;
            }
            if (isGu){ multiHandLandmarksStr += "\tGesture=GU\t"; }
            if (isPa){ multiHandLandmarksStr += "\tGesture=PA\t"; }
            ++handIndex;
        }

        return multiHandLandmarksStr;
    }

    /**
     * Description:	グーとパーだけを検出する
     */
    float[][] fingerPosition = new float[21][2];
    float[][] thumb = new float[4][2];
    float[][] indexFinger = new float[4][2];
    float[][] middleFinger = new float[4][2];
    float[][] ringFinger = new float[4][2];
    float[][] pinkyFinger = new float[4][2];
    // 指のステータス Finger states
    private boolean thumbIsOpen = false;
    private boolean indexFingerIsOpen = false;
    private boolean middleFingerIsOpen = false;
    private boolean ringFingerIsOpen = false;
    private boolean pinkyFingerIsOpen = false;
    private float fixKeyPoint;
    Intent UiIntent;
    private void isGUorPA(List<NormalizedLandmarkList> multiHandLandmarks){
        Log.d("isGUorPA", "starts");
        isGu=false; isPa=false;
        //only process if there is only one hand visible
        if (multiHandLandmarks.size() == 1) {
            for (NormalizedLandmarkList landmarks : multiHandLandmarks) {
                Log.d("isGUorPA", "\t#Hand positions: " + landmarks.getLandmarkCount() + ".");
                int landmarkIndex = 0;
                for (NormalizedLandmark landmark : landmarks.getLandmarkList()) {
                    fingerPosition[landmarkIndex][0] = landmark.getY();
                    fingerPosition[landmarkIndex][1] = landmark.getX();
                    ++landmarkIndex;
                }
            }
            //from fingerPositions to named fingers
            for (int i = 0; i < 4; i++) {
                thumb[i][0] = fingerPosition[i+1][0];  thumb[i][1] = fingerPosition[i+1][1];
                indexFinger[i][0] = fingerPosition[i+5][0];  indexFinger[i][1] = fingerPosition[i+5][1];
                middleFinger[i][0] = fingerPosition[i+9][0];  middleFinger[i][1] = fingerPosition[i+9][1];
                ringFinger[i][0] = fingerPosition[i+13][0];  ringFinger[i][1] = fingerPosition[i+13][1];
                pinkyFinger[i][0] = fingerPosition[i+17][0];  pinkyFinger[i][1] = fingerPosition[i+17][1];
            }
            //fingers open or not
            if (indexFinger[2][1] > indexFinger[1][1]) {
                indexFingerIsOpen = true;
            } else { indexFingerIsOpen = false; }
            if (middleFinger[2][1]> middleFinger[1][1]) {
                middleFingerIsOpen = true;
            } else { middleFingerIsOpen = false; }
            if (ringFinger[2][1] > ringFinger[1][1]) {
                ringFingerIsOpen = true;
            } else { ringFingerIsOpen = false; }
            if (pinkyFinger[2][1] > pinkyFinger[1][1]) {
                pinkyFingerIsOpen = true;
            } else { pinkyFingerIsOpen = false; }
            // ジェスチャー認識 Hand gesture recognition, if it's GU or PA, sends a local broadcast to the UI
            if (indexFingerIsOpen && middleFingerIsOpen && ringFingerIsOpen && pinkyFingerIsOpen) {
                Log.d("isGUorPA", "OPEN HAND");
                UiIntent = new Intent("jp.co.sundenshi.acereal.handservice");
                UiIntent.putExtra("gesture", "PA");
                isPa = true;
                LocalBroadcastManager.getInstance(this).sendBroadcast(UiIntent);
            } else if (!indexFingerIsOpen && !middleFingerIsOpen && !ringFingerIsOpen && !pinkyFingerIsOpen) {
                Log.d("isGUorPA", "FIST");
                UiIntent = new Intent("jp.co.sundenshi.acereal.handservice");
                UiIntent.putExtra("gesture", "GU");
                isGu=true;
                LocalBroadcastManager.getInstance(this).sendBroadcast(UiIntent);
            }else{
                Log.d("isGUorPA", "finger12345="+thumbIsOpen + indexFingerIsOpen + middleFingerIsOpen + ringFingerIsOpen + pinkyFingerIsOpen);
                UiIntent = new Intent("jp.co.sundenshi.acereal.handservice");
                UiIntent.putExtra("gesture", "NONE");
                LocalBroadcastManager.getInstance(this).sendBroadcast(UiIntent);
            }
        } else {
            UiIntent = new Intent("jp.co.sundenshi.acereal.handservice");
            UiIntent.putExtra("gesture", "NONE");
            LocalBroadcastManager.getInstance(this).sendBroadcast(UiIntent);
        }
    }

    /**
     * Description:	外部ファイルによるCamServiceのON/OFF（一定時間後繰り返し処理）
     * Author:	熊 Bill
     * Date:	2020/04/17
     */
    private Runnable fileCheck = new Runnable() {
        @Override
        public void run() {
            Pattern pattern;
            Matcher matcher;

            File file = new File(mExtDir + OUTER_COMMAND_FILE);
            try {
                if (!file.exists()) {
                    String strFileContent = "CAMERA_POWER=SLEEP\n" + "CAMERA_ID=" + userCamId + "\nOVERLAY=ON\n";
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(strFileContent.getBytes());
                    fos.close();
                } else {  //ユーザーカメラファイルが存在するので、読み込みます user camera file exists, reads it
                    long modifiedTime = file.lastModified();
                    if (modifiedTime != lastModifiedTime) {
                        lastModifiedTime = modifiedTime;

                        byte[] commandByte = Utility.loadFile(mExtDir + OUTER_COMMAND_FILE);
                        String command = "";
                        if (commandByte != null) command = new String(commandByte);
                        if (command.length() > 0) {
                            Log.d("[DEBUG]", "commandByte=" + command);
                            //if CAMERA_POWER==ON, カメラサービスを起動する launch camera service
                            pattern = Pattern.compile("CAMERA_POWER=ON");
                            matcher = pattern.matcher(command);
                            while (matcher.find()) {
                                Log.d("DEBUG", "FOUND: [" + matcher.group() + "], starting at index: " +
                                        matcher.start() + ", ends at: " + matcher.end());
                                startCameraService();
                                builder = new NotificationCompat.Builder(getApplicationContext());
                                builder.setContentTitle("Hand Tracking Service");
                                builder.setContentText("Hand Tracking Service ON");
                                builder.setSmallIcon(R.drawable.hand_tracking);
                                builder.setColor(Color.BLACK);

                                notificationManager = (NotificationManager)
                                        getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.notify(ONGOING_NOTIFICATION_ID, builder.build());
                            }
                            //if CAMERA_POWER==OFF, カメラサービスを終了する destroy camera service
                            pattern = Pattern.compile("CAMERA_POWER=OFF");
                            matcher = pattern.matcher(command);
                            while (matcher.find()) {
                                Log.d("DEBUG", "FOUND: [" + matcher.group() + "], starting at index: " +
                                        matcher.start() + ", ends at: " + matcher.end());
                                //stop camera
                                onDestroy();
                                //put the CAMERA_POWER to SLEEP in config file
                                initCameraFile(new File(mExtDir + OUTER_COMMAND_FILE));

                                builder = new NotificationCompat.Builder(getApplicationContext());
                                builder.setContentTitle("Hand Tracking Service");
                                builder.setContentText("Hand Tracking Service OFF");
                                builder.setSmallIcon(R.drawable.hand_not_tracking);
                                builder.setColor(Color.BLACK);

                                notificationManager = (NotificationManager)
                                        getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.notify(ONGOING_NOTIFICATION_ID, builder.build());
                            }
                            pattern = Pattern.compile("POSITION=([0-9]+),([0-9]+)");
                            matcher = pattern.matcher(command);
                            while (matcher.find()) {
                                Log.d("DEBUG", "FOUND: " + matcher.group() + ", starting at index: " +
                                        matcher.start() + ", ends at : " + matcher.end());
                                Log.d("DEBUG", "FOUND: " + matcher.group(matcher.groupCount()-1) + " and " + matcher.group(matcher.groupCount()));
                                ViewGroup.MarginLayoutParams marginParams = (ViewGroup.MarginLayoutParams) textureView.getLayoutParams();
                                marginParams.width = 90;
                                marginParams.topMargin = Integer.parseInt(matcher.group(matcher.groupCount()-1));
                                marginParams.leftMargin = Integer.parseInt(matcher.group(matcher.groupCount()));
                            }
                        }
                    }
                    fileHandler.removeCallbacks(this);
                    fileHandler.postDelayed(this, CHECK_INTERVAL);
                }
            }catch (IOException e) {
                Log.e("ERROR", e.toString());
            }
        }
    };

    private void initCameraFile(File file){
        String strFileContent = "CAMERA_POWER=SLEEP\n" + "CAMERA_ID=" + userCamId + "\nOVERLAY=";
        if (overlayIsOn) {
            strFileContent = strFileContent + "ON\n";
        } else {
            strFileContent = strFileContent + "OFF\n";
        }

        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(strFileContent.getBytes());
            fos.close();
        } catch (IOException e) {
            Log.e("ERROR", e.toString());
        }
    }


}