package jp.co.sundenshi.acereal.handservice.homelauncher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * ClassName:	Utility
 * Description:	便利関数を集めた
 */
public class Utility
{
	/**
	 * Description:		テキストファイルを読み込む
	 *
	 * Auther:	achaikun
	 * Date:	2016/11/21
	 *
	 * @param	filename	short named parameter description
	 * @param	_default	short named parameter description
	 * @return	String		ファイルの内容
	 * 						_default が返ってきたら、エラー発生中（ファイルがない）
	 */
	/**
	 * Description:
	 * バイナリファイルの読み込み
	 *
	 * Auther:	achaikun
	 * Date:	2016/12/12
	 *
	 * @param	filename	ファイル名
	 * @return	byte[]		バイナリデータ
	 */
	public static byte[] loadFile(String filename)
	{
		byte[] buffer = new byte[1];
		FileInputStream inputStream = null;
		BufferedInputStream bufferInput = null;
		ByteArrayOutputStream outputStream = null;
		try
		{
			inputStream = new FileInputStream(Utility.surroundByDoubleQuart(filename));
			bufferInput = new BufferedInputStream(inputStream);
			outputStream = new ByteArrayOutputStream();
			while (bufferInput.read(buffer) > 0)
			{
				outputStream.write(buffer);
			}
			outputStream.close();
			bufferInput.close();
			buffer = outputStream.toByteArray();
		} catch (FileNotFoundException e) {
			buffer = null;
			try
			{
				if (bufferInput != null)	bufferInput.close();
				if (inputStream != null) 	inputStream.close();
				if (outputStream != null)	outputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (IOException e) {
			buffer = null;
			try
			{
				if (bufferInput != null)	bufferInput.close();
				if (inputStream != null) 	inputStream.close();
				if (outputStream != null)	outputStream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}

		return buffer;
	}

	/**
	 * Description:
	 * ファイルに半角スペースがある可能性をみてダブルクオートで囲む
	 *
	 * Auther:	achaikun
	 * Date:	2017/01/24
	 *
	 * @param	filename	ファイル名
	 * @return	String		"ファイル名"
	 */
	public static String surroundByDoubleQuart(String filename)
	{
		String DQ = "\"";
		String ret = "";
		if (filename.indexOf(" ") >= 0)		ret = DQ + filename + DQ;
		else								ret = filename;
		return ret;
	}
}
