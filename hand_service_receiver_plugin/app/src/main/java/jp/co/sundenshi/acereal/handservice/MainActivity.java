package jp.co.sundenshi.acereal.handservice;

import android.Manifest;
import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.mediapipe.components.PermissionHelper;

//import jp.co.sundenshi.acereal.acerealhome.AceRealHomeUtility;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import jp.co.sundenshi.acereal.handservice.CamService;
import jp.co.sundenshi.acereal.handservice.R;
import jp.co.sundenshi.acereal.utility.AcerealUtility;

/// <summary>
/// camServiceを起動するためのメインアクティビティ The MainActivity to start camService
/// </summary>
public class MainActivity extends AppCompatActivity {

    public static final String CHANNEL_ID = "cameraService";
    public static final String CHANNEL_NAME = "Hand Input Channel";
    public static final int OVERLAY_REQ_CODE = 1000; //just a req id, just need to be unique
    public static final int CAMERA_PERMISSION_REQ_CODE = 1003;
    public static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 1004;
    public static boolean isFirstBoot = false;
    public static boolean isFirstENT = true;

    //UIの切替
    Button buttonStop;
    boolean isOn = false;
    TextView textView;

    //Write to setting file to turn on camera
    private static final String mExtDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/jp.co.sundenshi/acereal/";//ファイルパスのルート file path root
    private static final String OUTER_COMMAND_FILE = "jp.co.sundenshi.acereal.handservice.hand_tracking_camera_setting";

    //UIでジェスチャーのグーとパーの認識した時、テキスト表示
    BroadcastReceiver resultReceiver;

    //パーミッションガイドのイメージ
    ImageView permissionImageVIew;
    ImageView blankView;

    /// <summary>
    /// 起動するとき権限チェックと終了ボタンを配置 Check for permission and place a stop button at the beginning
    /// </summary>
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.button_layout);
        stopCameraFile(new File(mExtDir + OUTER_COMMAND_FILE)); //put the CAMERA_POWER to OFF then SLEEP in config file

        // カメラ背景処理(サービス)のSTOPボタン Stop button for camera service
        textView = (TextView) this.findViewById(R.id.textView);
        buttonStop = (Button) this.findViewById(R.id.button_stop);
        buttonStop.setText("START (ENTキー：ハンドトラッキング開始)");
        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //最初はENTキーを待って、ENTキーが来たら、パーミッション許可画面が出る
                if (isFirstENT){
                    isFirstENT = false;
                    requestPermission();
                    return;
                }

                if (!isOn) {
                    Toast.makeText(getApplicationContext(), "START HAND TRACKING", Toast.LENGTH_LONG).show();
                    buttonStop.setText("STOP (ENTキー: ハンドトラッキング停止)");
                    buttonStop.setEnabled(false);
                    textView.setText("カメラ起動中...");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            buttonStop.setEnabled(true);
                            textView.setText("ハンドトラッキング認識中...");
                        }
                    }, 4000);
                    startCameraFile(new File(mExtDir + OUTER_COMMAND_FILE));
                    isOn = true;
                } else {
                    Toast.makeText(getApplicationContext(), "STOP HAND TRACKING SERVICE", Toast.LENGTH_LONG).show();
                    buttonStop.setText("START (ENTキー：ハンドトラッキング開始)");
                    buttonStop.setEnabled(false);
                    textView.setText("カメラ終了中...");
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            buttonStop.setEnabled(true);
                            textView.setText("ハンドトラッキングを開始してください");
                        }
                    }, 5000);
                    //put the CAMERA_POWER to OFF then SLEEP in config file
                    stopCameraFile(new File(mExtDir + OUTER_COMMAND_FILE));
                    isOn = false;
                }
            }
        });

        //カメラの使用権限がなけらば、リクエストします。
        // if there's no permission to use the camera, ask for permission
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Camera permission OK", Toast.LENGTH_SHORT).show();
            //３つのパーミッション許可をとったら、アプリに戻るとき、ここをもう一度通すので、許可ガイド画面を消すが必要
            permissionImageVIew = findViewById(R.id.permission_guide);
            if (permissionImageVIew!=null) {permissionImageVIew.setVisibility(View.GONE);}
            blankView = findViewById(R.id.blank);
            if (blankView!=null) {blankView.setVisibility(View.GONE);}
        } else {
            isFirstBoot = true;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQ_CODE);
        }

        //onCreate()の最後にサービス側のジェスチャテキストのbroadcast receiverを構築 setup the broadcast receiver for detecting gesture at the end of onCreate()
        resultReceiver = createBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(resultReceiver, new IntentFilter("jp.co.sundenshi.acereal.handservice"));
    }
    private void requestPermission(){

        // 通知チャネルを作成、Android O (API 26) 以降必要 create a notification channel, needed after API 26 Android O
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT
            );
            channel.setLightColor(Color.BLUE);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        } else {
            // それ以前のバージョンなら不要 if it's an earlier version than that, do nothing, not needed
        }

        //ファイルのRead/Writeの権限リクエスト request read/write file access
        int writeExternalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if(writeExternalStoragePermission!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
        }else{
        }

        //カメラを起動(OverlayパーミッションOK）start the camera with the overlay permission
        if (!isFirstBoot) {//初期起動のとき、onActivityResult()でカメラ起動します。 The first boot relies on onActivityResult to start camera
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //after API 26 Android O/8
                        Intent intent = new Intent(getApplication(), CamService.class);
                        startForegroundService(intent); //need API 26, Android O
                    } else { // それ以前なバージョンなら before that
                        Intent intent = new Intent(this, CamService.class);
                        startService(intent);
                    }
                }
            }else{
                //カメラ許可がないなら、カメラを起動しません
            }
        }

        // 端末起動時のアプリ起動のときはActivityをすぐに非表示にする Hide the UI and launch home
        Intent intent = getIntent();
        if (intent.getBooleanExtra(Intent.ACTION_BOOT_COMPLETED, false)) {
            intent.putExtra(Intent.ACTION_BOOT_COMPLETED, false);
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeIntent);
        }
    }

    /// <summary>
    /// ユーザーの権限操作結果が来たら対応 Handle permission operations from users
    /// </summary>
    @Override
    public void onRequestPermissionsResult(
            int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d("START", "onRequestPermissionsResult()");

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {
            //最初はENTキーを待って、ENTキーが来たら、パーミッション許可画面が出る
            if (!isFirstENT) {
                //Overlay許可の前に許可ガイド画像を消す
                permissionImageVIew = findViewById(R.id.permission_guide);
                if (permissionImageVIew != null) { permissionImageVIew.setVisibility(View.GONE); }
            } else {
                //最初のカメラ許可結果で、BLANK画面を消す
                blankView = findViewById(R.id.blank);
                if (blankView!=null) {blankView.setVisibility(View.GONE);}
            }
        }else{
            //カメラ許可取れないので、もう一同リクエストする
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQ_CODE);
            return;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d("START", "onActivityResult()");

        //パーミッション画像を消す
        permissionImageVIew = findViewById(R.id.permission_guide);
        if (permissionImageVIew!=null) {permissionImageVIew.setVisibility(View.GONE);}

        // パーミッションと取られたらグラスへ戻る Return to glass mode after getting permission
        if (android.os.Build.PRODUCT.equals("AceReal0100")) {
            AcerealUtility.initAceRealHome();
            AcerealUtility.setDimensionMode(AcerealUtility.TYPE_2D);
            Log.d("GLASS", "mode");
        }

        permissionImageVIew = findViewById(R.id.permission_guide);
        if (permissionImageVIew!=null) {permissionImageVIew.setVisibility(View.GONE);}

        // パーミッションと取られたらカメラサービスを起動 Start camera service after getting permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this)) {
            // フォアグランドサービスを始める start foreground service
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //after API 26 Android O/8
                Intent intent = new Intent(getApplication(), CamService.class);
                startForegroundService(intent); //need API 26, Android O/8
            } else { // それ以前なバージョンなら before that
                Intent intent = new Intent(this, CamService.class);
                startService(intent);
            }
        }
    }

    /// <summary>
    ///ホーム画面に戻ったら、カメラプレビューをなくす
    /// </summary>
    @Override
    protected void onPause(){
        super.onPause();
        stopCameraFile(new File(mExtDir + OUTER_COMMAND_FILE));
    }

    @Override
    protected void onResume() {
        super.onResume();

        //もう一度パーミッションをチェックする
        checkPermissionsGranted();

        //起動イベントをもう一度出すと、サービスをそれを監視してもう一度ONになります
        Intent intent = getIntent();
        if (intent.getBooleanExtra(Intent.ACTION_BOOT_COMPLETED, false)) {
            intent.putExtra(Intent.ACTION_BOOT_COMPLETED, false);
            Intent homeIntent = new Intent(Intent.ACTION_MAIN);
            homeIntent.addCategory(Intent.CATEGORY_HOME);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeIntent);
        }

        //Resumeの時はボタンとUIのテキストを初期化します。
        isOn = false;
        buttonStop = (Button) this.findViewById(R.id.button_stop);
        buttonStop.setText("START (ENTキー：ハンドトラッキング開始)");
        textView = (TextView) this.findViewById(R.id.textView);
        textView.setText("ハンドトラッキングを開始してください");
    }

    private void checkPermissionsGranted() {
        //最初はENTキーを待って、ENTキーが来たら、パーミッション許可画面が出る(return;)
        int permissionCheck;
        if (isFirstENT){
            //サービス初回起動しても、もしOverlayパーミッションが許可されたら、許可画像を隠す。
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.canDrawOverlays(this)) {
                permissionImageVIew = findViewById(R.id.permission_guide);
                if (permissionImageVIew != null) { permissionImageVIew.setVisibility(View.GONE); }
            }
            return;
        }
        //2回名のENTキー以降は、カメラの使用権限がなけらば、リクエストします。
        // if there's no permission to use the camera, ask for permission
        permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        blankView = findViewById(R.id.blank);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Camera permission OK", Toast.LENGTH_SHORT).show();
        } else {
            isFirstBoot = true;
            if (blankView!=null)  { blankView.setVisibility(View.VISIBLE); }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_REQ_CODE);
        }

        //2回目のENTキー以降は、ファイルのRead/Writeの権限リクエスト request read/write file access
        int writeExternalStoragePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        blankView = findViewById(R.id.blank);
        if(writeExternalStoragePermission!= PackageManager.PERMISSION_GRANTED) {
            if (blankView!=null)  { blankView.setVisibility(View.VISIBLE); }
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION);
        }else{
            if (blankView!=null)  { blankView.setVisibility(View.GONE); }
        }

        //2回目以降、もしOverlayパーミッションが許可ないなら、、許可画像をリクエストする。
        // Overlayの権限がなければ、リクエストします。Android M (API 23)以上が必要な処理
        // if there's no permission for Overlay, need to ask for permission, needed after API 23 Android M
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            blankView = findViewById(R.id.blank);
            if (!Settings.canDrawOverlays(this)) {
                //onResume()の時、OverlayパーミッションがNGなら、もう一度許可をリクエストする
                Log.d("OVERLAY", "onResume() > checkPermissionsGranted()");
                //Log.d("OVERLAY", "Permission dialog > onRequestPermissionsResult()");
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_REQ_CODE);
            } else {
                //onResume()の時、OverlayパーミッションがOKなら、コントローラーの表示ではなく、ARのHMDに戻る
                AcerealUtility.setDimensionMode(AcerealUtility.TYPE_2D);
                permissionImageVIew = findViewById(R.id.permission_guide);
                if (permissionImageVIew != null) { permissionImageVIew.setVisibility(View.GONE); }
            }
        } else {
            // それ以前のバージョンなら不要 if it's an earlier version than that, do nothing, not needed
        }
        
    }

    private void startCameraFile(File file){
        String strFileContent = "CAMERA_POWER=ON\nCAMERA_ID=2\nOVERLAY=ON\n";
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(strFileContent.getBytes());
            fos.close();
        } catch (IOException e) {
            Log.e("ERROR", e.toString());
        }
    }

    private void stopCameraFile(File file){
        String strFileContent = "CAMERA_POWER=OFF\nCAMERA_ID=2\nOVERLAY=ON";
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(strFileContent.getBytes());
            fos.close();
        } catch (IOException e) {
            Log.e("ERROR", e.toString());
        }
    }


    private BroadcastReceiver createBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String str = intent.getStringExtra("gesture");
                Log.d("GESTURE", str);
                TextView textView = (TextView) findViewById(R.id.textPAorGU);
                if (str.equals("GU")) {
                    textView.setText("「グー」を認識しました");
                } else if (str.equals("PA")) {
                    textView.setText("「パー」を認識しました");
                } else if (str.equals("NONE")) {
                    textView.setText(" ");
                }
            }
        };
    }

        @Override
    public void onBackPressed() {
        //ESCボタンを押されたら、何もしない
    }

    @Override
    public void onDestroy() {
        if (resultReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(resultReceiver);
        }

        super.onDestroy();
    }
}