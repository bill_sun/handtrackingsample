変更履歴 Change History

2020-04-07 AndroidとUnityコードをレビューのためのコミット


インストールと実行手順

１．ADBでアプリをインストール
adb install -r jp.co.sundenshi.acereal.handservice.apk
adb install -r HandUnitySample.apk
２．AceRealで起動
まず「HandTracking(jp.co.sundenshi.acereal.handservice)」をENTキーで起動する。
カメラを見えるようになる。
３．HOMEボタンを押す
４．「Hand Tracking Sample」というUnityアプリをENTキーで起動する。
手をカメラの範囲内に見えるところなら、トラッキングされます。
５. OKサインをしたら、二つスフィアが赤くなる。
６．アプリを終了なら、
「HOME」ボタンを押す、「HandTracking」を再起動して、更に「ENT」キーを押すと
カメラとハンドトラッキングをOFFする。  